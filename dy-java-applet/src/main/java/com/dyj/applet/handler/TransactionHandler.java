package com.dyj.applet.handler;

import com.dyj.applet.domain.QueryMerchantStatus;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.CreateSubMerchantDataVo;
import com.dyj.applet.domain.vo.ImageUploadImageIdVo;
import com.dyj.applet.domain.MerchantUrl;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;

import java.io.InputStream;

/**
 * 交易系统
 */
public class TransactionHandler extends AbstractAppletHandler{
    public TransactionHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 进件图片上传
     * @param imageType 图片格式，支持格式：jpg、jpeg、png
     * @param imageContent 图片二进制字节流，最大为2M
     * @return
     */
    public DySimpleResult<ImageUploadImageIdVo> merchantImageUpload(String imageType, InputStream imageContent){
        BaseTransactionMerchantQuery baseQuery = new BaseTransactionMerchantQuery();
        baseQuery.setClientKey(agentConfiguration.getClientKey());
        baseQuery.setTenantId(agentConfiguration.getTenantId());
        return getTransactionClient().merchantImageUpload(baseQuery, imageType, imageContent);
    }

    /**
     * 发起进件
     * @param body 发起进件请求值
     * @return
     */
    public DySimpleResult<CreateSubMerchantDataVo> createSubMerchantV3(CreateSubMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().createSubMerchantV3(body);
    }

    /**
     * 进件查询
     * @param body 进件查询请求值
     * @return
     */
    public DySimpleResult<QueryMerchantStatus> queryMerchantStatusV3(QueryMerchantStatusMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().queryMerchantStatusV3(body);
    }

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     * @param body 开发者获取小程序收款商户/合作方进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openAppAddSubMerchant(GetMerchantUrlMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openAppAddSubMerchant(body);
    }

    /**
     * 服务商获取小程序收款商户进件页面
     * @param body 服务商获取小程序收款商户进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openAddAppMerchant(GetMerchantUrlMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openAddAppMerchant(body);
    }

    /**
     * 服务商获取服务商进件页面
     * @param body 服务商获取服务商进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openSaasAddMerchant(OpenSaasAddMerchantMerchantQuery body){
        baseQuery(body);
        return getTransactionClient().openSaasAddMerchant(body);
    }

    /**
     * 服务商获取合作方进件页面
     * @param body 服务商获取合作方进件页面请求值
     * @return
     */
    public DySimpleResult<MerchantUrl> openSaasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery body) {
        baseQuery(body);
        return getTransactionClient().openSaasAddSubMerchant(body);
    }
}
