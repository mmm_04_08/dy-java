package com.dyj.applet.domain;

/**
 * <p>受益人信息</p><p>当所选渠道包含微信，商户类型merchant_type=2为企业，且受益人类型beneficiary_type不是法人时必填，参照下面beneficiary参数描述</p>
 */
public class CreateSubMerchantBeneficiary {

    /**
     * <p>1.当所选渠道包含微信、商户类型为企业时，需要填写此项</p><p>2.请按照证件上住址填写，若证件上无住址则按照实际住址填写</p> 选填
     */
    private String address;
    /**
     * <p>证件照副照片部分, 身份证的话，是国徽面 营业照可以不传副照 ,通过图片上传接口获取</p> 选填
     */
    private String back_pic_url;
    /**
     * <p>证件照开始时间yyyymmdd 20211231</p>
     */
    private String begin_date;
    /**
     * <p>证件照过期时间yyyymmdd 20211231 长期填: 99991231</p>
     */
    private String exp_date;
    /**
     * <p>证件照主照片的图片id信息，通过图片上传接口获取</p>
     */
    private String front_pic_url;
    /**
     * <p>证件号 与id_type对应</p>
     */
    private String id_no;
    /**
     * <p>证件类型枚举值</p><p>1: 身份证</p><p>2: 户口本</p><p>3: 护照</p><p>4: 军官证</p><p>5: 士兵证</p><p>6: 香港居民来往内地通行证</p><p>7: 台湾同胞来往内地通行证</p><p>8: 临时身份证</p><p>9: 外国人居留证</p><p>10: 警官证</p><p>11: 澳门同胞来往内地通行证</p><p>12: 港澳居住证</p><p>13: 台湾居住证</p><p>100: 其他证件类型（上面枚举没有符合的）</p>
     */
    private Integer id_type;
    /**
     * 证件人名字
     */
    private String name;

    public String getAddress() {
        return address;
    }

    public CreateSubMerchantBeneficiary setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getBack_pic_url() {
        return back_pic_url;
    }

    public CreateSubMerchantBeneficiary setBack_pic_url(String back_pic_url) {
        this.back_pic_url = back_pic_url;
        return this;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public CreateSubMerchantBeneficiary setBegin_date(String begin_date) {
        this.begin_date = begin_date;
        return this;
    }

    public String getExp_date() {
        return exp_date;
    }

    public CreateSubMerchantBeneficiary setExp_date(String exp_date) {
        this.exp_date = exp_date;
        return this;
    }

    public String getFront_pic_url() {
        return front_pic_url;
    }

    public CreateSubMerchantBeneficiary setFront_pic_url(String front_pic_url) {
        this.front_pic_url = front_pic_url;
        return this;
    }

    public String getId_no() {
        return id_no;
    }

    public CreateSubMerchantBeneficiary setId_no(String id_no) {
        this.id_no = id_no;
        return this;
    }

    public Integer getId_type() {
        return id_type;
    }

    public CreateSubMerchantBeneficiary setId_type(Integer id_type) {
        this.id_type = id_type;
        return this;
    }

    public String getName() {
        return name;
    }

    public CreateSubMerchantBeneficiary setName(String name) {
        this.name = name;
        return this;
    }

    public static CreateSubMerchantBeneficiaryBuilder builder(){
        return new CreateSubMerchantBeneficiaryBuilder();
    }

    public static final class CreateSubMerchantBeneficiaryBuilder {
        private String address;
        private String back_pic_url;
        private String begin_date;
        private String exp_date;
        private String front_pic_url;
        private String id_no;
        private Integer id_type;
        private String name;

        private CreateSubMerchantBeneficiaryBuilder() {
        }

        public static CreateSubMerchantBeneficiaryBuilder aCreateSubMerchantBeneficiary() {
            return new CreateSubMerchantBeneficiaryBuilder();
        }

        public CreateSubMerchantBeneficiaryBuilder address(String address) {
            this.address = address;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder backPicUrl(String backPicUrl) {
            this.back_pic_url = backPicUrl;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder beginDate(String beginDate) {
            this.begin_date = beginDate;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder expDate(String expDate) {
            this.exp_date = expDate;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder frontPicUrl(String frontPicUrl) {
            this.front_pic_url = frontPicUrl;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder idNo(String idNo) {
            this.id_no = idNo;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder idType(Integer idType) {
            this.id_type = idType;
            return this;
        }

        public CreateSubMerchantBeneficiaryBuilder name(String name) {
            this.name = name;
            return this;
        }

        public CreateSubMerchantBeneficiary build() {
            CreateSubMerchantBeneficiary createSubMerchantBeneficiary = new CreateSubMerchantBeneficiary();
            createSubMerchantBeneficiary.setAddress(address);
            createSubMerchantBeneficiary.setBack_pic_url(back_pic_url);
            createSubMerchantBeneficiary.setBegin_date(begin_date);
            createSubMerchantBeneficiary.setExp_date(exp_date);
            createSubMerchantBeneficiary.setFront_pic_url(front_pic_url);
            createSubMerchantBeneficiary.setId_no(id_no);
            createSubMerchantBeneficiary.setId_type(id_type);
            createSubMerchantBeneficiary.setName(name);
            return createSubMerchantBeneficiary;
        }
    }
}
