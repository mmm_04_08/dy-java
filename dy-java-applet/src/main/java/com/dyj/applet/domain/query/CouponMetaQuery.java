package com.dyj.applet.domain.query;

import com.dyj.applet.domain.CreateCouponMeta;
import com.dyj.common.domain.query.BaseQuery;

/**
 * 创建券模板请求值
 */
public class CouponMetaQuery <T> extends BaseQuery {

    private T coupon_meta;

    public T getCoupon_meta() {
        return coupon_meta;
    }

    public CouponMetaQuery<T> setCoupon_meta(T coupon_meta) {
        this.coupon_meta = coupon_meta;
        return this;
    }

    public static <T> CreateCouponMetaQueryBuilder<T> builder(){
        return new CreateCouponMetaQueryBuilder<>();
    }


    public static final class CreateCouponMetaQueryBuilder<T> {
        private T coupon_meta;
        private Integer tenantId;
        private String clientKey;

        private CreateCouponMetaQueryBuilder() {
        }

        public static <T> CreateCouponMetaQueryBuilder<T> aCreateCouponMetaQuery() {
            return new CreateCouponMetaQueryBuilder<>();
        }

        public CreateCouponMetaQueryBuilder<T> couponMeta(T couponMeta) {
            this.coupon_meta = couponMeta;
            return this;
        }

        public CreateCouponMetaQueryBuilder<T> tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CreateCouponMetaQueryBuilder<T> clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CouponMetaQuery<T> build() {
            CouponMetaQuery<T> couponMetaQuery = new CouponMetaQuery<>();
            couponMetaQuery.setCoupon_meta(coupon_meta);
            couponMetaQuery.setTenantId(tenantId);
            couponMetaQuery.setClientKey(clientKey);
            return couponMetaQuery;
        }
    }
}
