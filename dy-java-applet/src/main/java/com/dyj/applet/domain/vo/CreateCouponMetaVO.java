package com.dyj.applet.domain.vo;

/**
 * 创建券模板返回值
 */
public class CreateCouponMetaVO {

    /**
     * <p>抖音开放平台小程序券模板ID</p>
     */
    private String coupon_meta_id;

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public CreateCouponMetaVO setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }
}
