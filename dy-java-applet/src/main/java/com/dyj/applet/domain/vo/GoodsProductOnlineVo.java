package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ProductOnline;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 13:36
 **/
public class GoodsProductOnlineVo {
    /**
     * 用于查询下一页
     */
    private String next_cursor;
    /**
     * 是否有下一页
     */
    private Boolean has_more;

    private List<ProductOnline> product_online_list;

    public String getNext_cursor() {
        return next_cursor;
    }

    public void setNext_cursor(String next_cursor) {
        this.next_cursor = next_cursor;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

    public List<ProductOnline> getProduct_online_list() {
        return product_online_list;
    }

    public void setProduct_online_list(List<ProductOnline> product_online_list) {
        this.product_online_list = product_online_list;
    }
}
