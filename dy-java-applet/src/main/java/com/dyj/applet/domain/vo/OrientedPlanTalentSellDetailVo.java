package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.OrientedPlanTalentSellDetail;

import java.util.Map;

public class OrientedPlanTalentSellDetailVo {

    /**
     * 达人带货数据，以达人抖音号为key，带货数据为value
     */
    private Map<String, OrientedPlanTalentSellDetail> data;

    /**
     * 数据产出日期
     */
    private String date;

    public Map<String, OrientedPlanTalentSellDetail> getData() {
        return data;
    }

    public void setData(Map<String, OrientedPlanTalentSellDetail> data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
