package com.dyj.applet.domain;

import java.util.List;

/**
 * 法人信息
 */
public class CreateSubMerchantLegalPerson {

    /**
     * <p>请按照证件上住址填写，若证件上无住址则按照实际住址填写</p><p>当所选渠道包含微信、商户类型=2为企业时，需要填写此项</p> 选填
     */
    private String address;
    /**
     * <p>证件照开始时间yyyymmdd 20211231</p>
     */
    private String begin_date;
    /**
     * <p>证件照过期时间yyyymmdd 20211231 长期填: 99991231</p>
     */
    private String exp_date;
    /**
     * <p>证件号，与id_type对应的证件号</p>
     */
    private String id_no;
    /**
     * <p>证件类型枚举值</p><p>1: 身份证</p><p>2: 户口本</p><p>3: 护照</p><p>4: 军官证</p><p>5: 士兵证</p><p>6: 香港居民来往内地通行证</p><p>7: 台湾同胞来往内地通行证</p><p>8: 临时身份证</p><p>9: 外国人居留证</p><p>10: 警官证</p><p>11: 澳门同胞来往内地通行证</p><p>12: 港澳居住证</p><p>13: 台湾居住证</p><p>100: 其他证件类型（上面枚举没有符合的）</p>
     */
    private Integer id_type;
    /**
     * 证件人名字
     */
    private String name;
    /**
     * <p>证件照副照片部分, 身份证的话，是国徽面 营业照可以不传副照 ,参数格式详见下面 图片信息参数</p> 选填
     */
    private List<ChannelAndUrl> back_pic_url;
    /**
     * 证件照主照片
     */
    private List<ChannelAndUrl> front_pic_url;

    public String getAddress() {
        return address;
    }

    public CreateSubMerchantLegalPerson setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public CreateSubMerchantLegalPerson setBegin_date(String begin_date) {
        this.begin_date = begin_date;
        return this;
    }

    public String getExp_date() {
        return exp_date;
    }

    public CreateSubMerchantLegalPerson setExp_date(String exp_date) {
        this.exp_date = exp_date;
        return this;
    }

    public String getId_no() {
        return id_no;
    }

    public CreateSubMerchantLegalPerson setId_no(String id_no) {
        this.id_no = id_no;
        return this;
    }

    public Integer getId_type() {
        return id_type;
    }

    public CreateSubMerchantLegalPerson setId_type(Integer id_type) {
        this.id_type = id_type;
        return this;
    }

    public String getName() {
        return name;
    }

    public CreateSubMerchantLegalPerson setName(String name) {
        this.name = name;
        return this;
    }

    public List<ChannelAndUrl> getBack_pic_url() {
        return back_pic_url;
    }

    public CreateSubMerchantLegalPerson setBack_pic_url(List<ChannelAndUrl> back_pic_url) {
        this.back_pic_url = back_pic_url;
        return this;
    }

    public List<ChannelAndUrl> getFront_pic_url() {
        return front_pic_url;
    }

    public CreateSubMerchantLegalPerson setFront_pic_url(List<ChannelAndUrl> front_pic_url) {
        this.front_pic_url = front_pic_url;
        return this;
    }

    public CreateSubMerchantLegalPersonBuilder builder() {
        return new CreateSubMerchantLegalPersonBuilder();
    }

    public static final class CreateSubMerchantLegalPersonBuilder {
        private String address;
        private String begin_date;
        private String exp_date;
        private String id_no;
        private Integer id_type;
        private String name;
        private List<ChannelAndUrl> back_pic_url;
        private List<ChannelAndUrl> front_pic_url;

        private CreateSubMerchantLegalPersonBuilder() {
        }

        public CreateSubMerchantLegalPersonBuilder address(String address) {
            this.address = address;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder beginDate(String beginDate) {
            this.begin_date = beginDate;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder expDate(String expDate) {
            this.exp_date = expDate;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder idNo(String idNo) {
            this.id_no = idNo;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder idType(Integer idType) {
            this.id_type = idType;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder name(String name) {
            this.name = name;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder backPicUrl(List<ChannelAndUrl> backPicUrl) {
            this.back_pic_url = backPicUrl;
            return this;
        }

        public CreateSubMerchantLegalPersonBuilder frontPicUrl(List<ChannelAndUrl> frontPicUrl) {
            this.front_pic_url = frontPicUrl;
            return this;
        }

        public CreateSubMerchantLegalPerson build() {
            CreateSubMerchantLegalPerson createSubMerchantLegalPerson = new CreateSubMerchantLegalPerson();
            createSubMerchantLegalPerson.setAddress(address);
            createSubMerchantLegalPerson.setBegin_date(begin_date);
            createSubMerchantLegalPerson.setExp_date(exp_date);
            createSubMerchantLegalPerson.setId_no(id_no);
            createSubMerchantLegalPerson.setId_type(id_type);
            createSubMerchantLegalPerson.setName(name);
            createSubMerchantLegalPerson.setBack_pic_url(back_pic_url);
            createSubMerchantLegalPerson.setFront_pic_url(front_pic_url);
            return createSubMerchantLegalPerson;
        }
    }
}
