package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

/**
 * 修改主播发券权限状态请求值
 */
public class UpdateTalentCouponStatusQuery extends UserInfoQuery {


    /**
     * 账号类型：
     取值1时：talent_account 字段必填
     取值2是：open_id 字段必填
     */
    private Integer account_type;
    /**
     * 小程序appid
     */
    private String app_id;
    /**
     * 抖音开平券模板id
     */
    private String coupon_meta_id;

    /**
     * 期望变更的主播发券状态，1：上架（生效），2：下架（失效）
     */
    private Integer status;
    /**
     * 主播抖音号，account_type为1时必填 选填
     */
    private String talent_account;

    public Integer getAccount_type() {
        return account_type;
    }

    public UpdateTalentCouponStatusQuery setAccount_type(Integer account_type) {
        this.account_type = account_type;
        return this;
    }

    public String getApp_id() {
        return app_id;
    }

    public UpdateTalentCouponStatusQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public UpdateTalentCouponStatusQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public UpdateTalentCouponStatusQuery setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public UpdateTalentCouponStatusQuery setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public static UpdateTalentCouponStatusQueryBuilder builder(){
        return new UpdateTalentCouponStatusQueryBuilder();
    }

    public static final class UpdateTalentCouponStatusQueryBuilder {
        private Integer account_type;
        private String app_id;
        private String coupon_meta_id;
        private Integer status;
        private String talent_account;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private UpdateTalentCouponStatusQueryBuilder() {
        }

        public static UpdateTalentCouponStatusQueryBuilder anUpdateTalentCouponStatusQuery() {
            return new UpdateTalentCouponStatusQueryBuilder();
        }

        public UpdateTalentCouponStatusQueryBuilder accountType(Integer accountType) {
            this.account_type = accountType;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder status(Integer status) {
            this.status = status;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder talentAccount(String talentAccount) {
            this.talent_account = talentAccount;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public UpdateTalentCouponStatusQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public UpdateTalentCouponStatusQuery build() {
            UpdateTalentCouponStatusQuery updateTalentCouponStatusQuery = new UpdateTalentCouponStatusQuery();
            updateTalentCouponStatusQuery.setAccount_type(account_type);
            updateTalentCouponStatusQuery.setApp_id(app_id);
            updateTalentCouponStatusQuery.setCoupon_meta_id(coupon_meta_id);
            updateTalentCouponStatusQuery.setStatus(status);
            updateTalentCouponStatusQuery.setTalent_account(talent_account);
            updateTalentCouponStatusQuery.setOpen_id(open_id);
            updateTalentCouponStatusQuery.setTenantId(tenantId);
            updateTalentCouponStatusQuery.setClientKey(clientKey);
            return updateTalentCouponStatusQuery;
        }
    }
}
