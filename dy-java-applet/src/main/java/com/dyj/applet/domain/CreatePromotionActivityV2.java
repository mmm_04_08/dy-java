package com.dyj.applet.domain;

/**
 * 创建营销活动请求值
 */
public class CreatePromotionActivityV2 {

    /**
     * <p>描述对应营销活动的名称。</p><ul><li>为了提高信息检索效率，该名称需要保证唯一。在创建过程中平台会校验名称的唯一性</li></ul>
     */
    private String activity_name;
    /**
     * <p>营销活动关联的券模板ID。营销活动进入发放周期后，用户领券行为会消耗该关联券模板ID的库存</p>
     */
    private String coupon_meta_id;
    /**
     * <p>用户可以参与活动（领取小程序券）的次数</p><ul><li>当send_scene=1或4时，该字段只能为1</li><li>当send_scene=2或5时，该字段应大于0小于20</li></ul>
     */
    private Long receive_limit;
    /**
     * <p>营销活动的对应发放场景</p>
     */
    private Integer send_scene;
    /**
     * <p>当send_scene=4时，填写结构内的字段</p> 选填
     */
    private PromotionActivityImActivity<CreatePromotionActivityImActivityShareFissionConfig> im_activity;
    /**
     * <p>当send_scene=1时，填写结构内的字段</p> 选填
     */
    private PromotionActivityLiveActivity live_activity;
    /**
     * <p>当send_scene=5时，填写结构内的字段</p> 选填
     */
    private PromotionActivitySidebarActivity sidebar_activity;

    public String getActivity_name() {
        return activity_name;
    }

    public CreatePromotionActivityV2 setActivity_name(String activity_name) {
        this.activity_name = activity_name;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public CreatePromotionActivityV2 setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public Long getReceive_limit() {
        return receive_limit;
    }

    public CreatePromotionActivityV2 setReceive_limit(Long receive_limit) {
        this.receive_limit = receive_limit;
        return this;
    }

    public Integer getSend_scene() {
        return send_scene;
    }

    public CreatePromotionActivityV2 setSend_scene(Integer send_scene) {
        this.send_scene = send_scene;
        return this;
    }

    public PromotionActivityImActivity getIm_activity() {
        return im_activity;
    }

    public CreatePromotionActivityV2 setIm_activity(PromotionActivityImActivity im_activity) {
        this.im_activity = im_activity;
        return this;
    }

    public PromotionActivityLiveActivity getLive_activity() {
        return live_activity;
    }

    public CreatePromotionActivityV2 setLive_activity(PromotionActivityLiveActivity live_activity) {
        this.live_activity = live_activity;
        return this;
    }

    public PromotionActivitySidebarActivity getSidebar_activity() {
        return sidebar_activity;
    }

    public CreatePromotionActivityV2 setSidebar_activity(PromotionActivitySidebarActivity sidebar_activity) {
        this.sidebar_activity = sidebar_activity;
        return this;
    }

    public static CreatePromotionActivityV2QueryBuilder builder(){
        return new CreatePromotionActivityV2QueryBuilder();
    }

    public static final class CreatePromotionActivityV2QueryBuilder {
        private String activity_name;
        private String coupon_meta_id;
        private Long receive_limit;
        private Integer send_scene;
        private PromotionActivityImActivity im_activity;
        private PromotionActivityLiveActivity live_activity;
        private PromotionActivitySidebarActivity sidebar_activity;
        private Integer tenantId;
        private String clientKey;

        private CreatePromotionActivityV2QueryBuilder() {
        }

        public static CreatePromotionActivityV2QueryBuilder aCreatePromotionActivityV2Query() {
            return new CreatePromotionActivityV2QueryBuilder();
        }

        public CreatePromotionActivityV2QueryBuilder activityName(String activityName) {
            this.activity_name = activityName;
            return this;
        }

        public CreatePromotionActivityV2QueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public CreatePromotionActivityV2QueryBuilder receiveLimit(Long receiveLimit) {
            this.receive_limit = receiveLimit;
            return this;
        }

        public CreatePromotionActivityV2QueryBuilder sendScene(Integer sendScene) {
            this.send_scene = sendScene;
            return this;
        }

        public CreatePromotionActivityV2QueryBuilder imActivity(PromotionActivityImActivity imActivity) {
            this.im_activity = imActivity;
            return this;
        }

        public CreatePromotionActivityV2QueryBuilder liveActivity(PromotionActivityLiveActivity liveActivity) {
            this.live_activity = liveActivity;
            return this;
        }

        public CreatePromotionActivityV2QueryBuilder sidebarActivity(PromotionActivitySidebarActivity sidebarActivity) {
            this.sidebar_activity = sidebarActivity;
            return this;
        }

        public CreatePromotionActivityV2 build() {
            CreatePromotionActivityV2 createPromotionActivityV2 = new CreatePromotionActivityV2();
            createPromotionActivityV2.setActivity_name(activity_name);
            createPromotionActivityV2.setCoupon_meta_id(coupon_meta_id);
            createPromotionActivityV2.setReceive_limit(receive_limit);
            createPromotionActivityV2.setSend_scene(send_scene);
            createPromotionActivityV2.setIm_activity(im_activity);
            createPromotionActivityV2.setLive_activity(live_activity);
            createPromotionActivityV2.setSidebar_activity(sidebar_activity);
            return createPromotionActivityV2;
        }
    }
}
