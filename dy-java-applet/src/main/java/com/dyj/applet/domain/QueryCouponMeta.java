package com.dyj.applet.domain;

/**
 * 查询券模板返回值
 */
public class QueryCouponMeta {

    /**
     * <p>发券确认的回调url</p> 选填
     */
    private String callback_url;
    /**
     * <p>券的使用须知</p> 选填
     */
    private String consume_desc;
    /**
     * <p>当前券模版的去使用链接，用户领取后可在直播间、卡包等场景点击「去使用」后跳转到「该链接」</p> 选填
     */
    private String consume_path;
    /**
     * <p>抖音开放平台券模板ID</p>
     */
    private String coupon_meta_id;
    /**
     * <p>小程序券名称</p>
     */
    private String coupon_name;
    /**
     * <p>优惠金额，单位分</p> 选填
     */
    private Long discount_amount;
    /**
     * <p>优惠类型</p>
     */
    private Integer discount_type;
    /**
     * <p>商家券模板号</p>
     */
    private String merchant_meta_no;
    /**
     * <p>满减门槛金额，单位分</p> 选填
     */
    private Long min_pay_amount;
    /**
     * <p>对于本地商品和泛知识课程的映射ID</p> 选填
     */
    private String origin_id;
    /**
     * <p>券模板生效时间（即可领取开始时间）</p>
     */
    private Long receive_begin_time;
    /**
     * <p>券的领取须知</p> 选填
     */
    private String receive_desc;
    /**
     * <p>券模板过期时间（即可领取结束时间）</p>
     */
    private Long receive_end_time;
    /**
     * <p>权益券的关联类型</p> 选填
     */
    private Integer related_type;
    /**
     * <p>用户领券回调时「签名、加密内容」的<strong>密钥来源</strong>：</p>
     */
    private Integer secret_source;
    /**
     * <p>券模板当前状态</p> 选填
     */
    private Integer status;
    /**
     * <p>券模板最大库存，最大值 1000000000；此库存是该券可发放的总库存，所有发券人共享。</p>
     */
    private Long stock_number;
    /**
     * <p>用户领取的「券记录」的有效期开始时间（单位秒）</p> 选填
     */
    private Long valid_begin_time;
    /**
     * <p>用户领取的「券记录」的有效时长（单位秒）</p> 选填
     */
    private Long valid_duration;
    /**
     * <p>用户领取的「券记录」的有效期结束时间（单位秒）</p> 选填
     */
    private Long valid_end_time;
    /**
     * <p>用户领券后「该用户所领取的券记录」的有效期类型</p>
     */
    private Integer valid_type;

    public String getCallback_url() {
        return callback_url;
    }

    public QueryCouponMeta setCallback_url(String callback_url) {
        this.callback_url = callback_url;
        return this;
    }

    public String getConsume_desc() {
        return consume_desc;
    }

    public QueryCouponMeta setConsume_desc(String consume_desc) {
        this.consume_desc = consume_desc;
        return this;
    }

    public String getConsume_path() {
        return consume_path;
    }

    public QueryCouponMeta setConsume_path(String consume_path) {
        this.consume_path = consume_path;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public QueryCouponMeta setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public QueryCouponMeta setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public QueryCouponMeta setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public Integer getDiscount_type() {
        return discount_type;
    }

    public QueryCouponMeta setDiscount_type(Integer discount_type) {
        this.discount_type = discount_type;
        return this;
    }

    public String getMerchant_meta_no() {
        return merchant_meta_no;
    }

    public QueryCouponMeta setMerchant_meta_no(String merchant_meta_no) {
        this.merchant_meta_no = merchant_meta_no;
        return this;
    }

    public Long getMin_pay_amount() {
        return min_pay_amount;
    }

    public QueryCouponMeta setMin_pay_amount(Long min_pay_amount) {
        this.min_pay_amount = min_pay_amount;
        return this;
    }

    public String getOrigin_id() {
        return origin_id;
    }

    public QueryCouponMeta setOrigin_id(String origin_id) {
        this.origin_id = origin_id;
        return this;
    }

    public Long getReceive_begin_time() {
        return receive_begin_time;
    }

    public QueryCouponMeta setReceive_begin_time(Long receive_begin_time) {
        this.receive_begin_time = receive_begin_time;
        return this;
    }

    public String getReceive_desc() {
        return receive_desc;
    }

    public QueryCouponMeta setReceive_desc(String receive_desc) {
        this.receive_desc = receive_desc;
        return this;
    }

    public Long getReceive_end_time() {
        return receive_end_time;
    }

    public QueryCouponMeta setReceive_end_time(Long receive_end_time) {
        this.receive_end_time = receive_end_time;
        return this;
    }

    public Integer getRelated_type() {
        return related_type;
    }

    public QueryCouponMeta setRelated_type(Integer related_type) {
        this.related_type = related_type;
        return this;
    }

    public Integer getSecret_source() {
        return secret_source;
    }

    public QueryCouponMeta setSecret_source(Integer secret_source) {
        this.secret_source = secret_source;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public QueryCouponMeta setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Long getStock_number() {
        return stock_number;
    }

    public QueryCouponMeta setStock_number(Long stock_number) {
        this.stock_number = stock_number;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public QueryCouponMeta setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_duration() {
        return valid_duration;
    }

    public QueryCouponMeta setValid_duration(Long valid_duration) {
        this.valid_duration = valid_duration;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public QueryCouponMeta setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public Integer getValid_type() {
        return valid_type;
    }

    public QueryCouponMeta setValid_type(Integer valid_type) {
        this.valid_type = valid_type;
        return this;
    }
}
