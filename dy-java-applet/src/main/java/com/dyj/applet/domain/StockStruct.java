package com.dyj.applet.domain;

/**
 * 库存信息
 * @author danmo
 * @date 2024-05-06 10:37
 **/
public class StockStruct {

    /**
     * 上限类型，为2时stock_qty和avail_qty字段无意义 1-有限库存 2-无限库存
     */
    private Integer limit_type;

    /**
     * 总库存，limit_type=2时无意义
     */
    private Integer stock_qty;

    public static StockStructBuilder builder() {
        return new StockStructBuilder();
    }

    public static class StockStructBuilder {
        private Integer limitType;
        private Integer stockQty;
        public StockStructBuilder limitType(Integer limitType) {
            this.limitType = limitType;
            return this;
        }
        public StockStructBuilder stockQty(Integer stockQty) {
            this.stockQty = stockQty;
            return this;
        }
        public StockStruct build() {
            StockStruct stockStruct = new StockStruct();
            stockStruct.setLimit_type(limitType);
            stockStruct.setStock_qty(stockQty);
            return stockStruct;
        }
    }

    public Integer getLimit_type() {
        return limit_type;
    }

    public void setLimit_type(Integer limit_type) {
        this.limit_type = limit_type;
    }

    public Integer getStock_qty() {
        return stock_qty;
    }

    public void setStock_qty(Integer stock_qty) {
        this.stock_qty = stock_qty;
    }
}
