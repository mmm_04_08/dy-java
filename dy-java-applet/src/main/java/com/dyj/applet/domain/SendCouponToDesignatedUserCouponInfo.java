package com.dyj.applet.domain;

/**
 * 开发者接口发券返回值 券信息
 */
public class SendCouponToDesignatedUserCouponInfo {

    /**
     * 使用须知 选填
     */
    private String consume_desc;
    /**
     * 核销url 选填
     */
    private String consume_path;
    /**
     * 券Id
     */
    private String coupon_id;
    /**
     * 券名称
     */
    private String coupon_name;
    /**
     * 券状态
     */
    private Integer coupon_status;
    /**
     * 优惠金额 选填
     */
    private Long discount_amount;
    /**
     * 券优惠类型
     */
    private Integer discount_type;
    /**
     * 商家模板号
     */
    private String merchant_meta_no;
    /**
     * 最少支付金额 选填
     */
    private Long min_pay_amount;
    /**
     * 领取须知 选填
     */
    private String receive_desc;
    /**
     * 领取时间
     */
    private Long receive_time;
    /**
     * 达人抖音号 选填
     */
    private String talent_account;
    /**
     * 达人OpenId 选填
     */
    private String talent_open_id;
    /**
     * 有效开始时间
     */
    private Long valid_begin_time;
    /**
     * 有效结束时间
     */
    private Long valid_end_time;

    public String getConsume_desc() {
        return consume_desc;
    }

    public SendCouponToDesignatedUserCouponInfo setConsume_desc(String consume_desc) {
        this.consume_desc = consume_desc;
        return this;
    }

    public String getConsume_path() {
        return consume_path;
    }

    public SendCouponToDesignatedUserCouponInfo setConsume_path(String consume_path) {
        this.consume_path = consume_path;
        return this;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public SendCouponToDesignatedUserCouponInfo setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
        return this;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public SendCouponToDesignatedUserCouponInfo setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
        return this;
    }

    public Integer getCoupon_status() {
        return coupon_status;
    }

    public SendCouponToDesignatedUserCouponInfo setCoupon_status(Integer coupon_status) {
        this.coupon_status = coupon_status;
        return this;
    }

    public Long getDiscount_amount() {
        return discount_amount;
    }

    public SendCouponToDesignatedUserCouponInfo setDiscount_amount(Long discount_amount) {
        this.discount_amount = discount_amount;
        return this;
    }

    public Integer getDiscount_type() {
        return discount_type;
    }

    public SendCouponToDesignatedUserCouponInfo setDiscount_type(Integer discount_type) {
        this.discount_type = discount_type;
        return this;
    }

    public String getMerchant_meta_no() {
        return merchant_meta_no;
    }

    public SendCouponToDesignatedUserCouponInfo setMerchant_meta_no(String merchant_meta_no) {
        this.merchant_meta_no = merchant_meta_no;
        return this;
    }

    public Long getMin_pay_amount() {
        return min_pay_amount;
    }

    public SendCouponToDesignatedUserCouponInfo setMin_pay_amount(Long min_pay_amount) {
        this.min_pay_amount = min_pay_amount;
        return this;
    }

    public String getReceive_desc() {
        return receive_desc;
    }

    public SendCouponToDesignatedUserCouponInfo setReceive_desc(String receive_desc) {
        this.receive_desc = receive_desc;
        return this;
    }

    public Long getReceive_time() {
        return receive_time;
    }

    public SendCouponToDesignatedUserCouponInfo setReceive_time(Long receive_time) {
        this.receive_time = receive_time;
        return this;
    }

    public String getTalent_account() {
        return talent_account;
    }

    public SendCouponToDesignatedUserCouponInfo setTalent_account(String talent_account) {
        this.talent_account = talent_account;
        return this;
    }

    public String getTalent_open_id() {
        return talent_open_id;
    }

    public SendCouponToDesignatedUserCouponInfo setTalent_open_id(String talent_open_id) {
        this.talent_open_id = talent_open_id;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public SendCouponToDesignatedUserCouponInfo setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public SendCouponToDesignatedUserCouponInfo setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }
}
