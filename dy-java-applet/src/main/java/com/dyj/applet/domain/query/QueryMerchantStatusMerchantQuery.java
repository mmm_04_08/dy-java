package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.enums.TransactionMerchantTokenTypeEnum;

/**
 * 进件查询请求值
 */
public class QueryMerchantStatusMerchantQuery extends BaseTransactionMerchantQuery {

    /**
     * <p>小程序 AppID，在开发者模式下必填</p> 选填
     */
    private String app_id;
    /**
     * <p>抖音开放平台分配的商户号，由<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/ecpay/apply/create" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">发起进件请求</a>接口返回</p>
     */
    private String merchant_id;
    /**
     * <p>商户 id，用于接入方自行标识并管理进件方。对应<a href="https://developer.open-douyin.com/docs/resource/zh-CN/mini-app/develop/server/ecpay/apply/create" target="_blank" rel="nofollow" class="syl-link" elementtiming="element-timing">发起进件请求</a>接口送入的sub_merchant_id</p> 选填
     */
    private String sub_merchant_id;
    /**
     * <p>服务商第三方小程序应用id，在服务商模式下必填</p> 选填
     */
    private String thirdparty_id;

    public String getApp_id() {
        return app_id;
    }

    public QueryMerchantStatusMerchantQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public QueryMerchantStatusMerchantQuery setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
        return this;
    }

    public String getSub_merchant_id() {
        return sub_merchant_id;
    }

    public QueryMerchantStatusMerchantQuery setSub_merchant_id(String sub_merchant_id) {
        this.sub_merchant_id = sub_merchant_id;
        return this;
    }

    public String getThirdparty_id() {
        return thirdparty_id;
    }

    public QueryMerchantStatusMerchantQuery setThirdparty_id(String thirdparty_id) {
        this.thirdparty_id = thirdparty_id;
        return this;
    }

    public static QueryMerchantStatusQueryBuilder builder(){
        return new QueryMerchantStatusQueryBuilder();
    }

    public static final class QueryMerchantStatusQueryBuilder {
        private String app_id;
        private String merchant_id;
        private String sub_merchant_id;
        private String thirdparty_id;
        private Integer tenantId;
        private String clientKey;
        private TransactionMerchantTokenTypeEnum transactionMerchantTokenType = TransactionMerchantTokenTypeEnum.CLIENT_TOKEN;

        private QueryMerchantStatusQueryBuilder() {
        }


        public QueryMerchantStatusQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public QueryMerchantStatusQueryBuilder merchantId(String merchantId) {
            this.merchant_id = merchantId;
            return this;
        }

        public QueryMerchantStatusQueryBuilder subMerchantId(String subMerchantId) {
            this.sub_merchant_id = subMerchantId;
            return this;
        }

        public QueryMerchantStatusQueryBuilder thirdpartyId(String thirdpartyId) {
            this.thirdparty_id = thirdpartyId;
            return this;
        }

        public QueryMerchantStatusQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryMerchantStatusQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryMerchantStatusQueryBuilder transactionMerchantTokenType(TransactionMerchantTokenTypeEnum transactionMerchantTokenType) {
            this.transactionMerchantTokenType = transactionMerchantTokenType;
            return this;
        }

        public QueryMerchantStatusMerchantQuery build() {
            QueryMerchantStatusMerchantQuery queryMerchantStatusQuery = new QueryMerchantStatusMerchantQuery();
            queryMerchantStatusQuery.setApp_id(app_id);
            queryMerchantStatusQuery.setMerchant_id(merchant_id);
            queryMerchantStatusQuery.setSub_merchant_id(sub_merchant_id);
            queryMerchantStatusQuery.setThirdparty_id(thirdparty_id);
            queryMerchantStatusQuery.setTenantId(tenantId);
            queryMerchantStatusQuery.setClientKey(clientKey);
            queryMerchantStatusQuery.setTransactionMerchantTokenType(transactionMerchantTokenType);
            return queryMerchantStatusQuery;
        }
    }
}
