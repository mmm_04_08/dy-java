package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-04-28 10:51
 **/
public class OrderCizStatusQuery extends BaseQuery {

    private String app_id;

    private String open_id;

    private String order_id;

    public static OrderCizStatusQueryBuilder builder() {
        return new OrderCizStatusQueryBuilder();
    }

    public static class OrderCizStatusQueryBuilder {
        private String appId;
        private String openId;
        private String orderId;
        private Integer tenantId;
        private String clientKey;
        public OrderCizStatusQueryBuilder appId(String appId) {
            this.appId = appId;
            return this;
        }
        public OrderCizStatusQueryBuilder openId(String openId) {
            this.openId = openId;
            return this;
        }
        public OrderCizStatusQueryBuilder orderId(String orderId) {
            this.orderId = orderId;
            return this;
        }
        public OrderCizStatusQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public OrderCizStatusQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public OrderCizStatusQuery build() {
            OrderCizStatusQuery orderCizStatusQuery = new OrderCizStatusQuery();
            orderCizStatusQuery.setApp_id(appId);
            orderCizStatusQuery.setOpen_id(openId);
            orderCizStatusQuery.setOrder_id(orderId);
            orderCizStatusQuery.setTenantId(tenantId);
            orderCizStatusQuery.setClientKey(clientKey);
            return orderCizStatusQuery;
        }
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
