package com.dyj.applet.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.backend.ContentType;
import com.dyj.applet.domain.QueryMerchantStatus;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.CreateSubMerchantDataVo;
import com.dyj.applet.domain.vo.ImageUploadImageIdVo;
import com.dyj.applet.domain.MerchantUrl;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.query.BaseTransactionMerchantQuery;
import com.dyj.common.interceptor.TransactionMerchantTokenInterceptor;

import java.io.InputStream;

/**
 * 交易系统
 */
@BaseRequest(baseURL = "${domain}")
public interface TransactionClient {

    /**
     * 进件图片上传
     * @param imageType 图片格式，支持格式：jpg、jpeg、png
     * @param imageContent 图片二进制字节流，最大为2M
     * @return
     */
    @Post(value = "${merchantImageUpload}", interceptor = TransactionMerchantTokenInterceptor.class,contentType = ContentType.MULTIPART_FORM_DATA)
    DySimpleResult<ImageUploadImageIdVo> merchantImageUpload(@Var("query") BaseTransactionMerchantQuery query, @Body(name = "image_type") String imageType, @DataFile(value = "image_content",fileName = "image_content") InputStream imageContent);


    /**
     * 发起进件
     * @param body 发起进件请求值
     * @return
     */
    @Post(value = "${createSubMerchantV3}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<CreateSubMerchantDataVo> createSubMerchantV3(@JSONBody CreateSubMerchantMerchantQuery body);

    /**
     * 进件查询
     * @param body 进件查询请求值
     * @return
     */
    @Post(value = "${queryMerchantStatusV3}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<QueryMerchantStatus> queryMerchantStatusV3(@JSONBody QueryMerchantStatusMerchantQuery body);

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     * @param body 开发者获取小程序收款商户/合作方进件页面请求值
     * @return
     */
    @Post(value = "${openAppAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openAppAddSubMerchant(@JSONBody GetMerchantUrlMerchantQuery body);

    /**
     * 服务商获取小程序收款商户进件页面
     * @param body 服务商获取小程序收款商户进件页面请求值
     * @return
     */
    @Post(value = "${openAddAppMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openAddAppMerchant(@JSONBody GetMerchantUrlMerchantQuery body);

    /**
     * 服务商获取服务商进件页面
     * @param body 服务商获取服务商进件页面请求值
     * @return
     */
    @Post(value = "${openSaasAddMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openSaasAddMerchant(@JSONBody OpenSaasAddMerchantMerchantQuery body);

    /**
     * 服务商获取合作方进件页面
     * @param body 服务商获取合作方进件页面请求值
     * @return
     */
    @Post(value = "${openSaasAddSubMerchant}", interceptor = TransactionMerchantTokenInterceptor.class)
    DySimpleResult<MerchantUrl> openSaasAddSubMerchant(@JSONBody OpenSaasAddSubMerchantMerchantQuery body);
}
