package com.dyj.examples.applet;

import com.alibaba.fastjson.JSON;
import com.dyj.applet.DyAppletClient;
import com.dyj.applet.domain.query.*;
import com.dyj.examples.DyJavaExamplesApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 交易系统测试
 */
@EnableAutoConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DyJavaExamplesApplication.class)
public class TransactionTest {


    /**
     * 进件图片上传
     */
    @Test
    public void merchantImageUpload(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.merchantImageUpload("jpg",null)
                )
        );
    }

    /**
     * 发起进件
     */
    @Test
    public void createSubMerchantV3(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.createSubMerchantV3(CreateSubMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 进件查询
     */
    @Test
    public void queryMerchantStatusV3(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.queryMerchantStatusV3(QueryMerchantStatusMerchantQuery.builder()
                                .merchantId("1")
                                .appId("1")
                                .build())
                )
        );
    }

    /**
     * 开发者获取小程序收款商户/合作方进件页面
     */
    @Test
    public void openAppAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openAppAddSubMerchant(GetMerchantUrlMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取小程序收款商户进件页面
     */
    @Test
    public void openAddAppMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openAddAppMerchant(GetMerchantUrlMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取服务商进件页面
     */
    @Test
    public void openSaasAddMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openSaasAddMerchant(OpenSaasAddMerchantMerchantQuery.builder().build())
                )
        );
    }

    /**
     * 服务商获取合作方进件页面
     */
    @Test
    public void openSaasAddSubMerchant(){
        DyAppletClient dyAppletClient = new DyAppletClient();
        System.out.println(
                JSON.toJSONString(
                        dyAppletClient.openSaasAddSubMerchant(OpenSaasAddSubMerchantMerchantQuery.builder().build())
                )
        );
    }

}
