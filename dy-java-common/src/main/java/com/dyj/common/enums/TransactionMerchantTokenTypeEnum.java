package com.dyj.common.enums;

/**
 * 交易接口accessToken类型
 */
public enum TransactionMerchantTokenTypeEnum {
    /**
     * 服务商身份获取token链接
     */
    COMPONENT_ACCESS_TOKEN,

    /**
     * 非服务商身份获取token链接
     */
    CLIENT_TOKEN;


    TransactionMerchantTokenTypeEnum() {
    }



}
